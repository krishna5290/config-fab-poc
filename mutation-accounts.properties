# Node environment
NODE_ENV=production
#------------------------------------------------------------------------------
######## E. ENTITLEMENTS CONFIGURATION ######## ::: SECTION START
#
#### E1. Entitlement interface :::: Optional ####
# Possible values: mock | active
# Default: active
# mock: Reads entitlement profile from mock_entl.js in <projectRoot/src> folder
# active: Connects to Entitlement Service
AUTH_MODE=active

#### E2. System user entitlement :::: Required ####
# Possible values: true | false
# Default: false
# true: Indicates that the component is to be considered a system user in entitlements (eg. Gazetteer)
# false: End user entitlements apply
#
SYSTEM_USER_AUTH=true

######## E. ENTITLEMENTS CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## Q. QUERY STORE CONFIGURATION ######## ::: SECTION START
#
#### Q1. Query store parameters used in development/test :::: Optional ####
# Possible values: true | false
# Default: true
# The query store is used as a convenience to load a query/variable set in Graphiql
#
# LOAD_QUERY_STORE=true

#### Q2. Default query set to be loaded :::: Optional ####
# If not provided no query will be loaded
#
DEFAULT_QUERY_LOAD=

######## Q. QUERY STORE CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## A. APPLICATION CONFIGURATION ######## ::: SECTION START
#### IMPORTANT: This must correspond to the schema definition ####
#
# A1. Application name used in Quest GraphQL Schema as Interface name :::: Required ####
APP_NAME_ACCT=AccountServices

#### A2. Application code for resolver package ::::  Required ####
# The projection store index name must correspond to this code (with lower casing applied)
APP_CODE_ACCT=ACCT

#### A3. Application Connector for resolver package ::::  Required ####
# The projection store connector option to be used
APP_CONNECTOR_ACCT=elasticSearch

#### A4. Application Connector Protocol for resolver package ::::  Required ####
# The projection store connector protocol option to be used
# NOTE: This will be removed eventually
APP_CONN_PROTOCOL_ACCT=http

### If schema build scope is set as base only then do not pick up
# devDependencies for packs other than the root
# This is typically relevant for execution in standalone mode and not in aggregated mode
SCHEMA_BUILD_SCOPE = baseOnly

######## A. APPLICATION CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## P. PROJECTION STORE CONFIGURATION ######## ::: SECTION START
#
#### P1. CF Service Name for Elastisearch cluster :::: Optional ####
# Default value is elasticsearch
# This is a Quest instance level configuration and will apply to all
# component packakges which will retrieve their configurations
#
# CF_SVC_NAME_ELASTICSEARCH=elasticsearch

#### P2. CF Service Name for specific package :::: Optional ####
# This is only applicable at an individual component package level
# The specific package will bind to the service name specified
# When provided in the Quest configuration serves as an override
#
# Overrides can be provided with a suffix
# ENTL - EntitlementsService
# ACCT - AccountServices
# ORG - OrgService
# FXRATE - FxRatesService
# BUSCAL - BusinessCalendarService
# REFDATA - ReferenceDataService
# LIQI - LiquidityICLService
# LIQS - LiquiditySweepsService
# LIQC - LiquidityCommonService
# PAY - PaymentsService
# CNR - CNRService
#
# Examples
# CF_SVC_NAME_ELASTICSEARCH_ENTL=entl-els-projection
# CF_SVC_NAME_ELASTICSEARCH_ORG=org-els-projection

#### P3. ES Connection string :::: Optional ####
# This is only used in local development.
# Is not considered when deployed in CF
#
# Elasticsearch connection string
# e.g. https://myuser:pwd@esserver.com:9200
# ES_CONNSTRING_ACCT=http://localhost:9200

#### P4. ES Server Partition for indexes :::: Optional ####
# Default value: N/A (empty)
# Indexes will be created/accessed with this prefix
# e.g. if Partition has a value of "DIT" then the index for Account Services will be "dit.acct"
#
# ES_PARTITION=quest

#### P5. Response size (limit) for Search queries :::: Optional ####
# Default value: 2000
# Search queries will throttle the number of records retrieved based on this value
#
# ES_QUERY_LIMIT=2000

#### P6. Response size (limit) for Aggregate queries :::: Optional ####
# Default value: 50
# Aggregate queries will throttle the number of buckets retrieved based on this value
#
# ES_AGG_QUERY_LIMIT=50

#### P7. Timeout threshold in ms :::: Optional ####
# Default value: 30000 ms
#
# ES_TIMEOUT=30000

######## P. PROJECTION STORE CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## E. REGISTRY CONFIGURATION ######## ::: SECTION START
#
#### E1. Enable discovery :::: Optional ###
# Possible values: true | false
# Default value: true
#
# EUREKA_DISCOVERY_ENABLED=true

#### E2. Eureka discovery name :::: Optional ####
# Default value: digital-quest
# If there are multiple quest contexts supported this must be provided
#
# EUREKA_DISCOVERY_NAME=digital-quest

#### E3. Eureka service name :::: Optional ####
# Default value: digital-registry
# Eureka service name to connect for discovery services
#
# CF_SVC_NAME_DIGITAL_REGISTRY=digital-registry

#### E4. Eureka registration method :::: Optional ####
# Possible values: direct | route
# Default value: route
# Eureka registration supporting
# :: direct: Direct container access registering IP
# :: route: Route is registered
#
# EUREKA_REGISTRATION_METHOD=route

######## E. REGISTRY CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## L. LOG CONFIGURATION ######## ::: SECTION START

#### L1. Logging level :::: Optional ####
# Supported values are
# error - Only errors are logged
# info - Information logs are enabled
# debug - Debug info is included in the logs
# Default value: info
#
LOG_LEVEL=info

#### L2. Console logging :::: Optional ####
# Possible values: true | false
# Default value: true
# Directed to standard output
#
CONSOLE_LOG=true

#### L3. Logs directed to file :::: Optional ####
# Possible values: true | false
# Default value: false
# Directed to file
# Default location is /logs/quest.log
#
FILE_LOG=false

#### L4. File name for File Logs :::: Optional ####
# Will be available in logs folder
#
# FILE_LOG_NAME=quest.log

######## L. LOG CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## R. RESPONSE PARAMETER CONFIGURATION ######## ::: SECTION START
#
#### R1. Include error stack trace :::: Optional ####
# Possible values: true | false
# Default value: false
# Set to true if you require stack traces to be included in logs/ errors section in response
#
ERROR_STACK_TRACE=false

#### R2. Include error path :::: Optional ####
# Possible values: true | false
# Default value: false
# ERROR_PATH=true

#### R3. Include timer information in meta :::: Optional ####
# NOTE: To be made optional with default value as false
# Possible values: true | false
# Default value: false
# TIMER=false

######## R. RESPONSE PARAMETER CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## S. SECURITY CONFIGURATION ######## ::: SECTION START

#### S1. User claims location :::: Optional ####
# Possible values: bearer | header
# Default value: bearer
# :: bearer: bearer id_token in Authorization header
# :: header: custom igtb_headers - igtb_user/igtb_domain
#
USER_CLAIMS_LOCATION=bearer

#### S2. Validate User token :::: Optional ####
# Possible values: true | false
# Default value: true
# Verify signature and expiry of token
# NOTE: Must be able to toggle both of the above independently
#
USER_TOKEN_VALIDATION=false

#### S3. Verify Shared Secret :::: Required ####
# Possible values: true | false
# Verify shared secret for system inquiries (within the delivery tier)
#
# VERIFY_SHARED_SECRET=true

######## S. SECURITY CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## M. MISCELLANEOUS CONFIGURATION ######## ::: SECTION START

#### M1. Dependant packages :::: Optional ####
# NOTE: This will be phased out
#
DEP_PKGS=true

#### M2. GraphIql enabled :::: Optional ####
# NOTE: This is for development/test usage
#
GRAPHIQL_ENABLED=true

#### M3. Quest route prefix used in GraphIql :::: Optional ####
# Possible value: quest
# Default value: N/A (empty)
# Required to support Graphiql route in Gatekeeper
#
# QUEST_ROUTE_PREFIX=

#### M4. Apollo engine configuration :::: Optional ####
APOLLO_ENGINE=false
APOLLO_ENGINE_KEY=service:prasvenk007-DigitalQuest:SGRBE4lyBnr4yU4R09W-HA
APOLLO_TRACING=false
APOLLO_CACHE_CONTROL=false

#### M5. NewRelic APM Configuration :::: Optional ####
NEW_RELIC_LICENSE_KEY=9a0343bb1dd3aecf0b2da2e92302b503098cec38

#### M6. Development only :::: Conditionally required ####
# NOTE: This will be phased out eventually when JWT or SHARED SECRET will be required
# If token verification is not required then custom headers can be provided
# for user identity transmission in Request
#
DEV_USER=DHartm390
DEV_DOMAIN=premierGroup

#### M7. Codacy project token :::: Optional ####
# Required for development
# Integration with Codacy
#
CODACY_PROJECT_TOKEN=bc9536b1021845eab80e0739cb0c5129

#### M8. Greeting on Server Start :::: Optional ####
GREETING=Welcome

#### Mocking enabled :::: Required ####
MOCKS_ENABLED=false

######## M. MISCELLANEOUS CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## K. KAFKA CONFIGURATION ######## ::: SECTION END
#
# If component type is ingestion then additional ingestion related bootstrapping is required
QUEST_PACKAGE_TYPE=ingestion
# Kafka enabled
KAFKA_ENABLED=true
# Kafka service URI
SVC_URI_KAFKA=10.197.12.15:9092
# Consumer Group Id
KAFKA_CONSUMER_GROUPID=PUSH-MESSAGE-BALANCE-GRP
# Kafka Topic for consumption
KAFKA_SUBSCRIPTION_TOPIC=PUSH-MESSAGE-BALANCE-OUTPUT-TOPIC
# Kafka Retry Topic
KAFKA_RETRY_TOPIC=PUSH-MESSAGE-BALANCE-INPUT-RETRY-TOPIC
# Kafka processing retries
KAFKA_MAX_RETRIES=1
# Kafka DLQ Topic - For quarantining messages that failed processing
KAFKA_DLQ_TOPIC=PUSH-MESSAGE-BALANCE-DLQ-TOPIC
# Connection time out
KAFKA_CONN_TIMEOUT=30000
# Heartbeat Interval
KAFKA_HEARTBEAT_INTERVAL=3000
# Number of conn retries
KAFKA_CONN_RETRIES=5
# Ingestion Model
INGESTION_MODEL=Account
# message Context
KAFKA_MESG_CONTEXT=PUSH-MESSAGE-BALANCE
#------------------------------------------------------------------------------
ACCT_NAMESPACE='07c21a4a-2479-4926-9e01-d069b5b10d3f'
#------------------------------------------------------------------------------
######## X. RABBITMQ CONFIGURATION ######## ::: SECTION START
#
#### X1. RabbitMQ Server URI :::: Required ###
#
MQ_ENABLED=false
SVC_URI_RABBITMQ=
# RMQ_URI=amqp://dituser:dituser@ec2-52-211-125-22.eu-west-1.compute.amazonaws.com:5672/dittest

#### X2. RabbitMQ service name :::: Required ####
# Default value: rabbitmq
# RabbitMQ service name to connect for event publishing
#
CF_SVC_NAME_RABBITMQ=rabbitmq

#### X3. Exchange/DLE/Q/DLQ/Key/DLK Info :::: Required ####
# All values except the exchange will be within the control
# of the base application
# Allow configuration - this will permit setup of exchange/qs/bindings
RMQ_ALLOW_CONFIG=true
# Exchange
RMQ_EXCHANGE=cbxevents
# DL Exchange
RMQ_DLE=
# Queue
RMQ_QUEUE=ACCT-INGESTION-INPUT-QUEUE
# Routing Key
RMQ_KEY=Americas.USA.*
# DLQ
RMQ_DLQ=DLQ-ACCT-INGESTION-INPUT-QUEUE
# DLK
RMQ_DLKEY=
# Prefetch
RMQ_PREFETCH=25
# Consumer Tags
RMQ_TAG=quest
RMQ_INTERNAL_TAG=quest-acct
# Internal Exchange
RMQ_INTERNAL_EXCHANGE=acctevents
# Queue for internal Exchange
RMQ_INTERNAL_QUEUE=ACCT-FORWARDED-INPUT-QUEUE
# Use err routing logic
RMQ_USE_ERR_ROUTING=true
# Retry delay in ms
RMQ_RETRY_DELAY=750
#### x4. SSL Enable  :::: Required ####
# Default value: false
# Only true if RabbitMQ have ssl configuration
# RMQ_SSL_ENABLED=false

# RMQ_SSL_CERT_SELFSIGNED=true

#### K12. RabbitMQ Hostname Verification  :::: Required ####
# Default value: false
#
# RMQ_SSL_HOSTNAME_VERIFICATION=false

#### K13.  RabbitMQ SSL Client Certificate Location  :::: Required ####
# Default value: ../../certs/cacert.pem
#
# RMQ_SSL_CERT_LOCATION=../../certs/cacert_kafka.pem

#### X4. Application instance region :::: Required ####
#
APP_INSTANCE_REGION=Americas

#### X5. Application instance country :::: Required ####
#
APP_INSTANCE_COUNTRY=USA
ES_VERSION_SCHEME=7
ES_PARTITION=acct
ACCOUNT_NAMESPACE = 'a636c586-c5d8-4689-9889-f65ed6e3d9c1';
ES_CONNSTRING_GLOBAL=http://localhost:9200
BASE_CURRENCY=AED
DEFAULT_REF_CURRENCY=AED
BASE_CURRENCY_PADDING=2